<?php
namespace CSParadise\HLQuery\Facades;

use Illuminate\Support\Facades\Facade;

class HLQuery extends Facade
{
    /**
     * Gets the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'HLQuery';
    }
}
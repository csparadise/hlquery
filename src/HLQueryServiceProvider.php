<?php
namespace CSParadise\HLQuery;

use Illuminate\Support\ServiceProvider;

class HLQueryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('HLQuery', function () {
            return new HLQuery(config('app.hlds_ip'), config('app.hlds_port'), config('app.hlds_rcon_password'));
        });
    }
}
